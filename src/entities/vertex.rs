#[derive(Copy, Clone)]
pub struct Vertex {
    pub v_position: [f32; 2],
}
implement_vertex!(Vertex, v_position);
