#[macro_use]
extern crate glium;
extern crate nalgebra;
extern crate time;

use glium::DisplayBuild;
use glium::glutin::ElementState;
use glium::glutin::Event;
use glium::glutin::WindowBuilder;
use glium::glutin::VirtualKeyCode;
use glium::Program;
use glium::Surface;
use glium::VertexBuffer;
use nalgebra::Rotation2;
use nalgebra::ToHomogeneous;
use nalgebra::Vector1;

use entities::Vertex;

mod entities;

fn main() {
    let display = WindowBuilder::new().build_glium().unwrap();

    let screen_quad = vec![
        Vertex { v_position: [-1.0, -1.0] },
        Vertex { v_position: [1.0, -1.0] },
        Vertex { v_position: [1.0, 1.0] },
        Vertex { v_position: [-1.0, 1.0] },
    ];
    let vertex_buffer = VertexBuffer::new(&display, &screen_quad).unwrap();
    let primitve_type = glium::index::NoIndices(glium::index::PrimitiveType::TriangleFan);

    let program = Program::from_source(&display,
                                       include_str!("shaders/vs.glsl"),
                                       include_str!("shaders/fs.glsl"),
                                       None)
        .unwrap();

    // Houd de starttijd bij, hiermee bepalen we uiteindelijk de zoomfactor
    let timer = time::now();
    loop {
        // listing the events produced by the window and waiting to be received
        for ev in display.poll_events() {
            match ev {
                Event::Closed |
                Event::KeyboardInput(ElementState::Pressed, _, Some(VirtualKeyCode::Escape)) => {
                    return
                }
                _ => (),
            }
        }

        let elapsed = (time::now() - timer).num_milliseconds() as f32 / 10000.0;

        // Hierop kunnen we draw calls aanroepen
        let mut target = display.draw();
        target.clear_color(0.0, 0.0, 0.0, 1.0);
        target.draw(&vertex_buffer,
                  &primitve_type,
                  &program,
                  &uniform! {
                      rotation: *Rotation2::new(Vector1::new(elapsed * 1.5)).to_homogeneous().as_ref(),
                      zoom: 9f32.powf(elapsed),
                  },
                  &Default::default())
            .unwrap();
        target.finish().unwrap();
    }
}
