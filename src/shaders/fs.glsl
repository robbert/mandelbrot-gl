#version 420

in vec2 f_position;

out vec4 color;

uniform mat3 rotation;
uniform float zoom;

void main() {
  vec3 transformed = rotation * vec3(f_position, 1.0);

  vec2 zoomed = (transformed.xy + vec2(-0.71245, 0.3105) * (zoom - 1)) / (zoom);
  int max_recursion = 420 + int(log2(zoom * 16));

  float a = 0;
  float b = 0;
  float distance = 0;
  int mandelbrot = 0;

  while (distance < 4 && mandelbrot < max_recursion) {
    float old_a = a;
    float old_b = b;

    a = old_a * old_a - old_b * old_b + zoomed.x;
    b = 2 * old_a * old_b + zoomed.y;

    // Hier vermijden we een onnodige wortel
    distance = a * a + b * b;

    mandelbrot++;
  }

  float gray = pow(mandelbrot, 1.5) / pow(max_recursion, 1.2);
  color = vec4(gray, gray, gray, 1.0);
}
